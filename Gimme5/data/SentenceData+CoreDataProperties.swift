//
//  SentenceData+CoreDataProperties.swift
//  Gimme5
//
//  Created by Luigi Mazzarella on 20/02/2020.
//  Copyright © 2020 Luigi Mazzarella. All rights reserved.
//
//

import Foundation
import CoreData


extension SentenceData {

    @nonobjc public class func fetchRequest() -> NSFetchRequest<SentenceData> {
        return NSFetchRequest<SentenceData>(entityName: "SentenceData")
    }

    @NSManaged public var meaning: String?
    @NSManaged public var phraseData: String?
    @NSManaged public var tagData: String?

}
