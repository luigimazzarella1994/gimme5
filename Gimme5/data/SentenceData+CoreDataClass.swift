//
//  SentenceData+CoreDataClass.swift
//  Gimme5
//
//  Created by Luigi Mazzarella on 20/02/2020.
//  Copyright © 2020 Luigi Mazzarella. All rights reserved.
//
//

import Foundation
import CoreData

@objc(SentenceData)
public class SentenceData: NSManagedObject {
	override init(entity: NSEntityDescription, insertInto context: NSManagedObjectContext?) {
		super.init(entity: entity, insertInto: context)
	}
}
