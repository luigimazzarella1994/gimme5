//
//  ListOfSentences.swift
//  Gimme5
//
//  Created by Luigi Mazzarella on 20/02/2020.
//  Copyright © 2020 Luigi Mazzarella. All rights reserved.
//

import Foundation

struct ListOfSentence: Codable {
	var listSentences: [Sentences]
}
