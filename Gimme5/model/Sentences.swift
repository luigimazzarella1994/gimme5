//
//  Sentences.swift
//  Gimme5
//
//  Created by Luigi Mazzarella on 20/02/2020.
//  Copyright © 2020 Luigi Mazzarella. All rights reserved.
//

import Foundation

struct Sentence: Codable {
	
	let phrase: String
	let description: String
	let tag: String
	
}

struct Sentences: Codable {
	let sentence : Sentence
}
