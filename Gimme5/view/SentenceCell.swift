//
//  SentenceCell.swift
//  Gimme5
//
//  Created by Luigi Mazzarella on 29/02/2020.
//  Copyright © 2020 Luigi Mazzarella. All rights reserved.
//


import Foundation
import UIKit

class SentenceCell : UITableViewCell {
	@IBOutlet weak var Sentenza: UILabel!
	@IBOutlet weak var detailLable: UILabel!
	override open var frame: CGRect {
		get {
			return super.frame
		}
		set (newFrame) {
			var frame =  newFrame
			frame.origin.y += 10
			frame.origin.x += 10
			frame.size.height -= 15
			frame.size.width -= 2 * 10
			super.frame = frame
		}
	}
	
	override open func awakeFromNib() {
		super.awakeFromNib()
		layer.cornerRadius = 15
		layer.masksToBounds = false
	}
}
