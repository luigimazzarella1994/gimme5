//
//  CardFlip.swift
//  Gimme5
//
//  Created by Luigi Mazzarella on 26/02/2020.
//  Copyright © 2020 Luigi Mazzarella. All rights reserved.
//

import UIKit

class CardFlip: UIView {
	
	/*
	// Only override draw() if you perform custom drawing.
	// An empty implementation adversely affects performance during animation.
	override func draw(_ rect: CGRect) {
	// Drawing code
	}
	*/
	
	
	// Front View
	var frontView: UIView = UIView() {
		didSet {
			self.addSubview(frontView)
		}
	}
	
	// Back View
	var backView: UIView = UIView() {
		didSet {
			
		}
	}
	
	// Set animation duration for transition
	var animationDuration: Double = 0.5
	
	// Set transition option
	var transitionStyle: UIView.AnimationOptions = UIView.AnimationOptions.transitionFlipFromRight
	
	override init(frame: CGRect) {
		super.init(frame: frame)
		self.backgroundColor = UIColor.clear
	}
	
	required init?(coder aDecoder: NSCoder) {
		super.init(coder: aDecoder)
		self.backgroundColor = UIColor.clear
	}
	
	override func touchesEnded(_ touches: Set<UITouch>, with event: UIEvent?) {
		flipView()
	}
	
	
	func flipView()
	{
		UIView.transition(from: self.frontView.isDescendant(of: self) ? self.frontView : self.backView, to: self.frontView.isDescendant(of: self) ? self.backView : self.frontView, duration: animationDuration, options: transitionStyle) { (completed) in
		}
	}
	
}


