//
//  CoreDataController.swift
//  Gimme5
//
//  Created by Luigi Mazzarella on 20/02/2020.
//  Copyright © 2020 Luigi Mazzarella. All rights reserved.
//

import Foundation
import UIKit
import CoreData

class CoreDataController {
	static let shared = CoreDataController()
	
	private var context: NSManagedObjectContext

	
	private init() {
		let application = UIApplication.shared.delegate as! AppDelegate
		self.context = application.persistentContainer.viewContext
	}
	
	func addSentence(phraseData: String, meaning: String, tag: String){
		
		let entity = NSEntityDescription.entity(forEntityName: "SentenceData", in: self.context)
		
		let newSentence = SentenceData(entity: entity!, insertInto: self.context)
		newSentence.phraseData = phraseData
		newSentence.meaning = meaning
		newSentence.tagData = tag
		
		
		do {
			try self.context.save()
		} catch let errore {
			print("[CDC] Problema salvataggio Sentence: \(String(describing: newSentence.tagData)) in memoria")
			print("  Stampo l'errore: \n \(errore) \n")
		}
		
		print("[CDC] Sentence \(String(describing: newSentence.tagData)) salvato in memoria correttamente")
	}
	
	func loadFavourites() ->  [SentenceData]{
		
		var listOfSentence : [SentenceData] = []
		
		
		print("[CDC] Recupero tutte le sentences dal context ")
		
		let fetchRequest: NSFetchRequest<SentenceData> = SentenceData.fetchRequest()
		
		do {
			let array = try self.context.fetch(fetchRequest)
			
			guard array.count > 0 else {print("[CDC] Non ci sono elementi da leggere "); return listOfSentence}
			
			for x in array {
				let sentence = x
				print("[CDC] Sentence \(sentence.phraseData) - Descrizione \(sentence.meaning)")
				listOfSentence.append(sentence)
			}
			
		} catch let errore {
			print("[CDC] Problema esecuzione FetchRequest")
			print("  Stampo l'errore: \n \(errore) \n")
		}
		
		return listOfSentence
	}
	
	private func loadSentenceFromFetchRequest(request: NSFetchRequest<SentenceData>) -> [SentenceData] {
		var array = [SentenceData]()
		do {
			array = try self.context.fetch(request)
			
			guard array.count > 0 else {print("[CDC] Non ci sono elementi da leggere "); return []}
			
			for x in array {
				print("[CDC] phrase \(x.phraseData!) - detail \(x.meaning!)")
			}
			
		} catch let errore {
			print("[CDC] Problema esecuzione FetchRequest")
			print("  Stampo l'errore: \n \(errore) \n")
		}
		
		return array
	}
	
	
	func loadSentenceFromName(tag: String) -> SentenceData {
		let request: NSFetchRequest<SentenceData> = NSFetchRequest(entityName: "SentenceData")
		request.returnsObjectsAsFaults = false
		
		let predicate = NSPredicate(format: "tagData = %@", tag)
		request.predicate = predicate
		
		let sentence = self.loadSentenceFromFetchRequest(request: request)
		return sentence[0]
	}
	
	func deleteSentece(tag: String) {
		let sentence = self.loadSentenceFromName(tag: tag)
		self.context.delete(sentence)
		
		do {
			try self.context.save()
		} catch let errore {
			print("[CDC] Problema eliminazione libro ")
			print("  Stampo l'errore: \n \(errore) \n")
		}
	}
}
