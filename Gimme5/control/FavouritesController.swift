//
//  FavouritesController.swift
//  Gimme5
//
//  Created by Luigi Mazzarella on 29/02/2020.
//  Copyright © 2020 Luigi Mazzarella. All rights reserved.
//

import UIKit

class FavouritesController: UIViewController,UITableViewDelegate,UITableViewDataSource, UISearchResultsUpdating {

	
	@IBOutlet weak var myTable: UITableView!
	@IBOutlet weak var favouritesLable: UILabel!
	var resultSearchController: UISearchController?
	
	var sentences: [SentenceData] = []
	var filteredSentences: [SentenceData] = []
	var selectedIndex = -1
	var collapse = false
	
	
	override func viewDidLoad() {
		super.viewDidLoad()
		// Do any additional setup after loading the view.
		loadArray()
		
		resultSearchController =
			({
				let controller = UISearchController(searchResultsController: nil)
				controller.obscuresBackgroundDuringPresentation = true
				controller.searchResultsUpdater = self as? UISearchResultsUpdating
				controller.searchBar.sizeToFit()
				self.myTable.tableHeaderView = controller.searchBar
				
				return controller
				
			})()
		self.myTable.reloadData()
	}
	
	func updateSearchResults(for searchController: UISearchController) {
		print("Sto per iniziare la ricerca")
		self.filterList(researchText: searchController.searchBar.text!, scope: "All")
	}
	
	func filterList(researchText: String, scope: String = "All"){
		print("Sto filtrando i contenuti")
		filteredSentences.removeAll(keepingCapacity: true)
		for elem in sentences {
			var justOne = false
			if (elem.tagData)?.lowercased().range(of: researchText.lowercased()) != nil && justOne == false {
				filteredSentences.append(elem)
				justOne = false
			}
			
			self.myTable.reloadData()
		}
	}
	
	func loadArray(){
		sentences = CoreDataController.shared.loadFavourites()
	}
	
	func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
		// #warning Incomplete implementation, return the number of rows
		guard let controller = self.resultSearchController else{
			return 0
		}
		if controller.isActive {
			return self.filteredSentences.count
		} else {
			return self.sentences.count
		}
	}
	
	
	func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
		

		tableView.deselectRow(at: indexPath, animated: true)
		if selectedIndex == indexPath.row {
			if self.collapse == true {
				self.collapse = false
			}else{
				self.collapse = true
			}
		}else{
			self.collapse = true
		}
		self.selectedIndex = indexPath.row
		tableView.reloadRows(at: [indexPath], with: .automatic)
		
	}
	
	
	func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
		if selectedIndex == indexPath.row && collapse == true {
			return 180
		}
		else {
			return 70
		}
	}
	
	func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
		let cell = tableView.dequeueReusableCell(withIdentifier: "cella", for: indexPath) as! SentenceCell
		var sentenceInTable: SentenceData
		
		//         Configure the cell...
		if self.resultSearchController!.isActive {
			sentenceInTable = self.filteredSentences[indexPath.row]
		} else {
			//ricavo un elemento della lista in posizione row (il num di riga) e lo conservo
			sentenceInTable = self.sentences[indexPath.row]
		}
		
		cell.Sentenza.text = sentenceInTable.tagData
		cell.Sentenza.sizeToFit()
		cell.detailLable.text = sentenceInTable.meaning
		cell.detailLable.sizeToFit()
		cell.userInteractionEnabledWhileDragging = false
		cell.layer.cornerRadius=30
		cell.layer.masksToBounds=true
		cell.layer.borderWidth = 1
		
		
		
		return cell
	}
	
	func tableView(_ tableView: UITableView, commit editingStyle: UITableViewCell.EditingStyle, forRowAt indexPath: IndexPath) {
		
		if editingStyle == .delete {
			
			CoreDataController.shared.deleteSentece(tag: sentences[indexPath.row].tagData!)
			self.sentences.remove(at: indexPath.row)
			tableView.deleteRows(at:[indexPath] , with:.fade)
		}
		
	
		
	}
	
}


