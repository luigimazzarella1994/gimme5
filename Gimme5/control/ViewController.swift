//
//  ViewController.swift
//  Gimme5
//
//  Created by Luigi Mazzarella on 20/02/2020.
//  Copyright © 2020 Luigi Mazzarella. All rights reserved.
//

import UIKit
import UserNotifications

class ViewController: UIViewController {
	
	@IBOutlet weak var myView: CardFlip!
	@IBOutlet var first: UIView!
	@IBOutlet var second: UIView!
	@IBOutlet weak var sentenceLable: UILabel!
	@IBOutlet weak var tagLabel: UILabel!
	@IBOutlet weak var descriptionLabel: UILabel!
	@IBOutlet weak var preferButton: UIButton!
	@IBOutlet weak var heartNavigation: UIBarButtonItem!
	
	
	var sentences = ListOfSentence(listSentences: [])
	var sentencesOfTheDay = Sentence(phrase: "", description: "", tag: "")
	var count = 0
	var check = false
	var checkSentence: [SentenceData] = []
	var countDay = 0
	var lastDay: Date!
	var defaults = UserDefaults.standard
	
	override func viewDidLoad() {
		
		super.viewDidLoad()
		// Do any additional setup after loading the view.
		loadData()
		print(sentences)
		cardView()
		customNavigationBar()
		dailySentence()
		notification()
		dayCounter()
		print("Il contatore è \(countDay)")
		
		
	}
	override func viewWillAppear(_ animated: Bool) {
		isInCore()
	}
	
	func dayCounter()  {
		
		let formatter = DateFormatter()
		//2016-12-08 03:37:22 +0000
		formatter.dateFormat = "yyyy-MM-dd"
		let now = Date()
		print("Data non stringata \(now)")
		let dateString = formatter.string(from:now)
		print("Oggi è \(dateString)")
		if lastDay == nil {
			lastDay = now
			
		}
		let lastDayFormatter = formatter.string(from: lastDay)
		if lastDayFormatter != dateString {
			countDay += 1
			print("ieri era \(String(describing: lastDayFormatter)), mentre oggi è \(dateString)")
			lastDay = now
			print("é un giorno nuovo!!")
			
		}else{
			print("é lo stesso giorno")
		}
		
		defaults.set(lastDay, forKey: "Yesterday")
		defaults.set(countDay, forKey: "DayCounter")
		
		
	}
	
	func isInCore(){
		check = false
		checkSentence = CoreDataController.shared.loadFavourites()
		for sentence in checkSentence {
			if sentence.tagData == sentencesOfTheDay.tag {
				check = true
			}
			
		}
		if check {
			preferButton.setImage(UIImage(named: "heart2"), for: .normal)
			count = 1
			print("C'è già")
		}else{
			preferButton.setImage(UIImage(named: "heart-1"), for: .normal)
			count = 0
			print("non c'è")
		}
		
		
		print("ho verificato")
		
	}
	
	func notification(){
		
		let center = UNUserNotificationCenter.current()
		let options: UNAuthorizationOptions = [.alert, .sound]
		
		center.requestAuthorization(options: options) { (granted, error) in
			if !granted {
				print("Something went wrong")
			}
		}
		center.getNotificationSettings { (settings) in
			if settings.authorizationStatus != .authorized {
				// Notifications not allowed
			}
		}
		
		let content = UNMutableNotificationContent()
		content.title = "Daily Sentence"
		content.body = sentencesOfTheDay.phrase
		content.sound = UNNotificationSound.default
		
		let gregorian = Calendar(identifier: .gregorian)
		let now = Date()
		var components = gregorian.dateComponents([.year, .month, .day, .hour, .minute, .second], from: now)
		
		components.hour = 13
		components.minute = 51
		components.second = 0
		
		let date = gregorian.date(from: components)!
		
		let triggerDaily = Calendar.current.dateComponents([.hour, .minute, .second], from: date)
		let trigger = UNCalendarNotificationTrigger(dateMatching: triggerDaily, repeats: true)
		
		
		
		let identifier = "UYLLocalNotification"
		let request = UNNotificationRequest(identifier: identifier, content: content, trigger: trigger)
		
		center.add(request, withCompletionHandler: { (error) in
			if error != nil {
				// Something went wrong
				print(error ?? "Error")
			}
		})
		
	}
	
	func dailySentence() {
		
		sentenceLable.text = sentences.listSentences[countDay].sentence.phrase
		sentenceLable.lineBreakMode = NSLineBreakMode.byWordWrapping
		sentenceLable.sizeToFit()
		tagLabel.text = sentences.listSentences[countDay].sentence.tag
		descriptionLabel.text = sentences.listSentences[countDay].sentence.description
		descriptionLabel.sizeToFit()
		
		sentencesOfTheDay = sentences.listSentences[countDay].sentence
		
	}
	
	func cardView(){
//		self.myView.layer.cornerRadius = 30
//		self.myView.clipsToBounds = true
//		self.myView.layer.shadowPath =
//			UIBezierPath(roundedRect: self.myView.bounds,
//						 cornerRadius: self.myView.layer.cornerRadius).cgPath
//		self.myView.layer.shadowColor = UIColor.black.cgColor
//		self.myView.layer.shadowOpacity = 0.5
//		self.myView.layer.shadowOffset = CGSize(width: 10, height: 10)
//		self.myView.layer.shadowRadius = 1
//		self.myView.layer.masksToBounds = false
//		self.myView.layer.borderWidth = 0.5
		
		
		

		
		self.first.layer.cornerRadius = 30
		self.first.clipsToBounds = true
//		self.first.layer.shadowPath =
//			UIBezierPath(roundedRect: self.first.bounds,
//						 cornerRadius: self.first.layer.cornerRadius).cgPath
//		self.first.layer.shadowColor = UIColor.black.cgColor
//		self.first.layer.shadowOpacity = 0.5
//		self.first.layer.shadowOffset = CGSize(width: 10, height: 10)
//		self.first.layer.shadowRadius = 1
		self.first.layer.masksToBounds = false
		self.first.layer.borderWidth = 0.5
		self.first.backgroundColor = UIColor(patternImage: UIImage(named: "frontCard")!)
		
		
		self.second.layer.cornerRadius = 30
		self.second.clipsToBounds = true
//		self.second.layer.shadowPath =
//			UIBezierPath(roundedRect: self.second.bounds,
//						 cornerRadius: self.second.layer.cornerRadius).cgPath
//		self.second.layer.shadowColor = UIColor.black.cgColor
//		self.second.layer.shadowOpacity = 0.5
//		self.second.layer.shadowOffset = CGSize(width: 10, height: 10)
//		self.second.layer.shadowRadius = 1
		self.second.layer.masksToBounds = false
		self.second.layer.borderWidth = 0.5
		self.second.backgroundColor = UIColor(patternImage: UIImage(named: "backCard")!)
		
		self.myView.frontView = self.first
		self.myView.backView = self.second
		self.myView.transitionStyle = UIView.AnimationOptions.transitionFlipFromLeft
		
		
	}
	

	func loadData(){

		
		print("Sono qui")
		let sentenceJson = Bundle.main.url(forResource: "SentencesJson3", withExtension: "json")!
		do{
			let data = try Data(contentsOf: sentenceJson)
			let decoder: JSONDecoder = JSONDecoder.init()
			let user: ListOfSentence = try decoder.decode(ListOfSentence.self, from: data)
			
			self.sentences = user
		}catch let e {
			print(e)
		}

	}
	
	func customNavigationBar(){
		
		self.navigationController!.navigationBar.isTranslucent = true
		self.navigationController?.navigationBar.tintColor = .systemRed

	}
	
	

	
	@IBAction func addToPreferences(_ sender: Any) {
		
		
		if count == 0 {
			preferButton.setImage(UIImage(named: "heart2"), for: .normal)
			CoreDataController.shared.addSentence(phraseData: sentencesOfTheDay.phrase, meaning: sentencesOfTheDay.description, tag: sentencesOfTheDay.tag)
			count += 1
		}else {
			preferButton.setImage(UIImage(named: "heart-1"), for: .normal)
			CoreDataController.shared.deleteSentece(tag: sentencesOfTheDay.tag)
			count -= 1
		}

	}
	
}

